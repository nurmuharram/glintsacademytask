function tambah (angka1, angka2) {
    return angka1 + angka2;    
}
function kurang (angka1, angka2) {
    return angka1 - angka2;    
}
function kali (angka1, angka2) {
    return angka1 * angka2;    
}
function bagi (angka1, angka2) {
    return angka1 / angka2;    
}
    
export {tambah, kurang, kali, bagi}