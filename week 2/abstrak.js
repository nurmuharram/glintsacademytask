var AbstractClass = function() {  
    if (this.constructor === AbstractClass) {  
        throw new Error("Can't instantiate abstract class!");  
    }  
};  
AbstractClass.prototype.do = function() {  
    throw new Error("Abstract method!");  
}  
AbstractClass.prototype.type = function() {  
    console.log('this is a class');  
}