import {tambah, kurang, kali, bagi} from './operator';
const prompt = require('prompt-sync')();

let status = true;

while(status)
{
    let angka1 = parseInt( prompt("Masukkan pertama : ") ); 
    let angka2 = parseInt( prompt("Masukkan kedua : ") ); 
    let operator = prompt("Masukkan operator (tambah/kurang/kali/bagi) ");    

    switch(operator)
    {
        case "tambah":
            console.log("Hasil penjumlahan = "+tambah(angka1, angka2));
            break;
        case "kurang":
            console.log("Hasil penjumlahan = "+kurang(angka1, angka2));
            break;
        case "kali":
            console.log("Hasil penjumlahan = "+kali(angka1, angka2));
            break;
        case "bagi":
            console.log("Hasil penjumlahan = "+bagi(angka1, angka2));
            break;
        default:
            console.log("Operator tidak dikenali");
            break;
    }

    status = prompt("Hitung lagi ? (ya/tidak) ") == "ya" ? true : false ;
}