// persegi
// luas = sisi * sisi
// keliling = 4 * sisi

// peresegi panjang
// luas = p * l
// keliling = 2 * (p + l)

// jajar genjang
// luas = a * t
// keliling = 2 * (a + b)

// segitiga
// luas = (a * t) / 2
// keliling = a + b + c

class BangunDatar {
    constructor(s, p , l, a, t, b, c) {
    this.s = s;
    this.p = p;
    this.l = l;
    this.a = a;
    this.t = t;
    this.b = b;
    this.c = c;
    }
    //rumus persegi
    luasPersegi(s) {
    return "Luas Persegi = "+ s * s;
    } 

    kelilingPersegi(s) {
    return "Keliling Persegi = "+ 4 * s;
    }
    
    //rumus persegi panjang
    luasPersegiP(p,l) {
    return "Luas Persegi Panjang = "+ p * l;
    }

    kelilingPersegiP(p,l) {
        return "Keliling Persegi Panjang = "+ 2*(p+l);
        }

        //rumus jajar genjang
    luasJajarGenjang(a,t) {
        return "Luas Jajar Genjang = "+ a * t;
        }
    
        kelilingJajarGenjang(a,b) {
            return "Keliling Jajar Genjang = "+ 2 * (a + b);
            }

            //rumus segi tiga
    luasSegitiga(a,t) {
        return "Luas Segitiga = "+(a * t)/2;
        }
    
        kelilingSegitiga(a,b,c) {
            return "Keliling Segitiga = " + 2 * (a + b + c);
            }
};
    let persegi = new BangunDatar;
    let persegiPanjang = new BangunDatar;
    let jajarGenjang = new BangunDatar;
    let segitiga = new BangunDatar;
//Persegi
    console.log(persegi.luasPersegi(5));
    console.log(persegi.kelilingPersegi(5)); 
    console.log(persegiPanjang.luasPersegiP(12,5)); 
    console.log(persegiPanjang.kelilingPersegiP(12,5)); 
    console.log(jajarGenjang.luasJajarGenjang(12,5)); 
    console.log(jajarGenjang.kelilingJajarGenjang(12,5)); 
    console.log(segitiga.luasSegitiga(12,5));
    console.log(segitiga.kelilingSegitiga(12,12,12));