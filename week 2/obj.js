let ktp = {
    nama: "Sabrina",
    alamat: {
        jalan: "Jl. Kabupaten",
        "rt/rw": "05/010"
        },
        pekerjaan: [
            "Dosen UI",
            "Mentor Binar"
            ]
};

console.log(ktp.pekerjaan[0])
//console.log(ktp.nama) // Output: Sabrina
//console.log(ktp["alamat"]["rt/rw"]) // Output: 05/010
//console.log(ktp.alamat["rt/rw"]) // Output: 05/010
// Array di dalam Object
//ktp.pekerjaan.forEach(function(item) {
  //  console.log(`${item} adalah pekerjaan ${ktp.nama}`)
    //})
/* Output: Dosen UI adalah pekerjaan Sabrina */ /* Output: [Array[3], Array[3]] -> ["Sabana", "Sabrina",
/* Output: Mentor Binar adalah pekerjaan Sabrina */