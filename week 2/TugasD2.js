// arrow
let LuasLingkaran = (r) => 2 * 3.14 * r;
console.log('Luas lingkaran adalah ', LuasLingkaran(15));


// Function Expression
let LuasLingkaran1 = function(r) {
    return 2 * 3.14 * r
};
console.log('Luas lingkaran adalah ', LuasLingkaran1(13));

// Function declaration
function LuasLingkaran2(r) {
    return 2 * 3.14 * r
};
console.log('Luas lingkaran adalah ', LuasLingkaran2(12));

/// 2

