import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigation from './MainNavigation';
import LoginScreen from '../screens/LoginScreen';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';
const Stack = createStackNavigator();

export default function AppStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen
          name="Main"
          component={MainNavigation}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  statusisLogin : state.auth.isLoggedIn,
})

const mapDispatchToProps = (dispatch) => ({});

export default  connect (mapStateToProps,mapDispatchToProps)(LoginScreen);
