import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DashboardStack from './DashboardStack';
import NewsScreen from '../screens/NewsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import todos from '../screens/todos';

const Tab = createBottomTabNavigator();

export default function MainNavigation() {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          if (route.name === 'Dashboard') {
            iconName = 'home-outline';
          } else if (route.name === 'NewsScreen') {
            iconName = 'newspaper-outline';
          } else if (route.name === 'ProfileScreen') {
            iconName = 'person-outline';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#3B50EB',
        inactiveTintColor: 'black',
      }}>
      <Tab.Screen
        name="NewsScreen"
        component={NewsScreen}
        options={{tabBarLabel: 'News'}}
      />
      <Tab.Screen name="Dashboard" component={DashboardStack} />
      <Tab.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{tabBarLabel: 'Profile'}}
      />

      <Tab.Screen name='TODO' component={todos} />
    </Tab.Navigator>
  );
}
