import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import axios from 'axios';

export default class Weather extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listWeather: [],
      selectedWeather: {
        location: 'Lokasi',
        short_note: 'Kondisi',
        kelembaban: '0%',
        angin: '0km/h',
        presipitasi: '0%',
        suhu: '0°C',
      },
    };
  }

  componentDidMount() {
    // call axios
    axios({
      method: 'GET',
      url: 'http://localhost:5000/weather',
    })
      .then((res) => {
        this.setState({listWeather: res.data});
        this.timerWeather = setInterval(() => {
          const randomNumber = Math.floor(
            Math.random() * this.state.listWeather.length,
          );
          this.setState({
            selectedWeather: this.state.listWeather[randomNumber],
          });
        }, 5000);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  componentWillUnmount() {
    clearInterval(this.timerWeather);
  }

  render() {
    return (
      <View>
        <View style={styles.jokesHeader}>
          <Text>Weather Condition</Text>
          <Text>See All</Text>
        </View>
        <View style={styles.itemContainer}>
          <View style={styles.itemInfo}>
            <View>
              <Text>Suhu : {this.state.selectedWeather.suhu}</Text>
              <Text>Kelembapan : {this.state.selectedWeather.kelembaban}</Text>
            </View>
            <View>
              <Text>Angin : {this.state.selectedWeather.angin}</Text>
              <Text>
                Presipitasi : {this.state.selectedWeather.presipitasi}
              </Text>
            </View>
          </View>
          <Text style={styles.itemKondisi}>
            {this.state.selectedWeather.short_note}
          </Text>
          <Text style={styles.itemLokasi}>
            {this.state.selectedWeather.location}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  jokesHeader: {
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 10,
  },
  itemContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
    padding: 20,
    alignItems: 'stretch',
    height: 200,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 15,
  },
  itemLokasi: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontWeight: 'bold',
  },
  itemKondisi: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontStyle: 'italic',
    marginBottom: 10,
  },
});
