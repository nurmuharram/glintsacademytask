import React from 'react';
import {ScrollView, StyleSheet, View, Text, Image, Button} from 'react-native';
import PromotionItem from '../components/Promotion.component';
import Jokes from '../components/Jokes.component';
import Weather from '../components/Weather.component';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View style={styles.header}>
          <View style={styles.headerImage}>
            <Image
              source={{
                uri:
                  'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png',
              }}
              style={styles.profilImage}
            />
          </View>
          <View style={styles.headerTitle}>
            <Text style={styles.titleText}>Hi, Rendy</Text>
          </View>
          <View>
            <Button
              color="#444"
              onPress={() => this.props.navigation.navigate('Other')}
              title="Login"
            />
          </View>
        </View>
        <ScrollView>
          <View style={styles.body}>
            {/* Carousel */}
            <View>
              <PromotionItem />
            </View>
            {/* Jokes */}
            <View style={styles.itemJokes}>
              <Jokes />
            </View>
            {/* Weathert */}
            <View style={styles.itemJokes}>
              <Weather />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: 'white',
  },
  headerImage: {
    marginHorizontal: 10,
  },
  profilImage: {
    width: 30,
    height: 30,
  },
  headerTitle: {
    justifyContent: 'center',
  },
  titleText: {
    fontFamily: 'segoe-ui',
    fontWeight: 'bold',
    color: '#333333',
    fontSize: 15,
  },
  body: {
    padding: 10,
    height: 700,
    backgroundColor: '#FFFFFF',
  },
  itemJokes: {
    marginTop: 10,
  },
});
