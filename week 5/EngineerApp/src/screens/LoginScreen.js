import React , {useState} from 'react';
import {Text, View, Button,TextInput} from 'react-native';
import {connect} from 'react-redux';

export default function LoginScreen(props) {
  const [username,setUsername] =  useState(null);
  const [password,setPassword] =  useState(null);
  return (
    <View>
      <Text>Ayo Login</Text>
      <TextInput
        onChangeText = {(Text) => setUsername(Text)}
        value = {username}
        style = {{backgroundColor : '#ffff'}}
        
        />

        <TextInput
        onChangeText = {(Text) => setPassword(Text)}
        value = {password}
        style = {{backgroundColor : '#ffff'}}
        secureTextEntry={true}
      
        />
        <Button
        color = "#444"
        onPress = {() => props.navigation.navigate('Main')}
        title ='GO TO MAIN'></Button>

    </View>
  );
}

const mapStateToProps = (state) => ({}); // ngambilin data dari store
const mapDispatchToProps = (dispatch) => ({});

export default  connect (mapStateToProps,mapDispatchToProps)(LoginScreen);
