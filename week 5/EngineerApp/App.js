import React from 'react';
import {StatusBar} from 'react-native';
import AppStack from './src/navigations/AppStack';
import {Provider} from 'react-redux';

import store from './src/store';


export default function App() {
  return (
    <Provider store = {store}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <AppStack />
    </Provider>
  );
}
