class Kendaraan {
    constructor(nama, kecepatan, warna){
        this.nama = nama;
        this.kecepatan = kecepatan;
        this.warna = warna;
    }

    tampilkanNama(){
        console.log("Kendaraan ini merupakan sebuah ", this.nama);
    }

    tampilkanKecepatan() {
        console.log(this.nama, ' ini memiliki kecepatan hingga ', this.kecepatan);
    }

    tampilkanWarna() {
        console.log(this.nama, ' ini berwarna ', this.warna);
    }
    
    tampilkanSpesifikasi() {
        console.log("Spefikasi ", this.nama, ":");
        console.log("- kecepatan : ", this.kecepatan);
        console.log("- warna : ", this.warna);
    }
}

class Motor extends Kendaraan {
    constructor(nama, kecepatan, warna, roda){
        super(nama, kecepatan, warna);
        this.roda = roda
    }

    tampilkanRoda(){
        console.log(this.nama, ' ini beroda ', this.roda);
    }

    tampilkanSpesifikasi() {
        console.log("Spefikasi ", this.nama, ":");
        console.log("- kecepatan : ", this.kecepatan);
        console.log("- warna : ", this.warna);
        console.log("- roda : ", this.roda);
    }
}

class Mobil extends Kendaraan {
    constructor(nama, kecepatan, warna){
        super(nama, kecepatan, warna);
    }
}

const motor = new Motor('Motor', '125', 'biru', 'dua');
const mobil = new Mobil('Mobil', '250', 'merah');

// motor.tampilkanNama();
// motor.tampilkanKecepatan();
// motor.tampilkanWarna();
// motor.tampilkanRoda();

// mobil.tampilkanNama();
// mobil.tampilkanKecepatan();
// mobil.tampilkanWarna();

motor.tampilkanSpesifikasi();
mobil.tampilkanSpesifikasi();
 
