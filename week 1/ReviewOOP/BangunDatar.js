class BangunDatar{ // Abstract class
    constructor() { // constructor untuk set data awal
        if (this.constructor === BangunDatar) { // abstract tidak bisa diinisialisasi hanya bisa diturunkan
            throw new Error("Cannot instantiate from Abstract Class") // Because it's abstract
        }
        this.nama = this.constructor.name;
    }

    // enkapsulasi pada atribut public, porected, private
    // public dapat diakses seluruh class
    // protected diakses class sendiri dan turunannya
    // private hanya bisa diakses class berangkutan
    // getName() public, _getName() protected, #getName() private
    _getName = () => console.log(`Nama bangun datar ini ${this.nama}`);
}


class Persegi extends BangunDatar{ // menurunkan class abstract BangunDatar agar dapat mengakses atributnya
    constructor(sisi){ // constructor membutuhkan parameter sisi
        super(); // menginisiasi constructor parent sebagai konsekuensi extend
        this.sisi = sisi;
    }
    
    _getName = () => console.log("Overriding _getName() bangun datar"); // melakukan overriding dengan mendeklarasikan ulang fungsi

    keliling() { // membuat fungsi keliling persegi
        return 4 * this.sisi;
    }

    luas() { // membuat fungsi luas persegi
        return this.sisi * this.sisi;
    }    
}


class PersegiPanjang extends BangunDatar{ // menurunkan class abstract BangunDatar agar dapat mengakses atributnya
    constructor(panjang, lebar){ // constructor membutuhkan parameter panjang dan lebar
        super(); // menginisiasi constructor parent sebagai konsekuensi extend
        this.panjang = panjang;
        this.lebar = lebar;
    }

    keliling() { // membuat fungsi keliling persegi panjang
        return 2 * ( this.panjang + this.lebar );
    }

    luas() { // membuat fungsi luas persegi panjang
        return this.panjang * this.lebar;
    }    
}


class JajarGenjang extends BangunDatar{ // menurunkan class abstract BangunDatar agar dapat mengakses atributnya
    constructor(sisi_bawah, sisi_atas, tinggi){ // constructor membutuhkan parameter sisi_bawah, sisi_atas, tinggi        
        super(); // menginisiasi constructor parent sebagai konsekuensi extend
        this.sisi_bawah = sisi_bawah;
        this.sisi_atas = sisi_atas;
        this.tinggi = tinggi;
    }

    keliling = () => 2 * (this.sisi_bawah + this.sisi_atas); // membuat fungsi keliling jajargenjang

    luas() { // membuat fungsi luas jajargenjang
        return this.sisi_bawah * this.tinggi;
    }              
}

// Inisiasi dan menggunakan fungsi pada class Persegi
const persegiku = new Persegi(5);
console.log("Keliling : "+persegiku.keliling());
console.log("Luas : "+persegiku.luas());

// Inisiasi dan menggunakan fungsi pada class PersegiPanjang
const persegikPanjangku = new PersegiPanjang(10, 5);
console.log("Keliling : "+persegikPanjangku.keliling());
console.log("Luas : "+persegikPanjangku.luas());

// Inisiasi dan menggunakan fungsi pada class JajarGenjang
const jajarGenjangku = new JajarGenjang(10, 5, 15);
console.log("Keliling : "+jajarGenjangku.keliling());
console.log("Luas : "+jajarGenjangku.luas());
 
