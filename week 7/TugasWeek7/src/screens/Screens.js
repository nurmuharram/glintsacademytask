import React, { Component } from 'react';
import {StyleSheet,View,Text, TextInput, TouchableOpacity, Dimensions, Image} from 'react-native';
import { SocialIcon, Avatar, Accessory } from 'react-native-elements';
import LoginScreen from './LoginScreen';


const SignUpScreen =()=>{
   
    return (
      <View style={styles.container}>
        <View style={styles.contLogo} >
           <Avatar
              rounded
              size = 'large'
              source={{
                uri:
                  'https://pbs.twimg.com/profile_images/1263098058956345344/S9WHIGkq_400x400.jpg',
              }}
            >
            <Accessory/></Avatar>
            
        </View>
        <View style={styles.contInput}>
            <View 
                style={{
                backgroundColor: "",
                borderBottomColor:"white",
                borderBottomWidth:0.5,
                }}>             
                <TextInput
                placeholder='Name'
                placeholderTextColor="#fff" 
                style={styles.formInput}
                />
            </View>

            <View 
                style={{
                backgroundColor: "",
                borderBottomColor:"white",
                borderBottomWidth:0.5,
                }}>    
                <TextInput
                placeholder='Username'
                placeholderTextColor="#fff" 
                style={styles.formInput}
                
                />
            </View>
            
            <View 
                style={{
                backgroundColor: "",
                borderBottomColor:"white",
                borderBottomWidth:0.5,
                }}>  
            
                <TextInput
                placeholder='Email'
                placeholderTextColor="#fff" 
                style={styles.formInput}
                />
             </View>
             <View 
                style={{
                backgroundColor: "",
                borderBottomColor:"white",
                borderBottomWidth:0.5,
                }}>   
                <TextInput
                placeholder='Password'
                placeholderTextColor="#fff" 
                style={styles.formInput}
                />
             </View>
            <View style={styles.contButton}>
                
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>Sign Up</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.contText}>
                <Text style={{color:'#fff'}}>Already have an Account ?</Text>
                <TouchableOpacity onPress={LoginScreen}>
                    <Text style={{color:'#e09f3e', fontWeight:'bold'}}>Sign In</Text>
                </TouchableOpacity>
                <Text style={{color:'#fff', marginTop:10}}>or Sign Up With</Text>
            </View>
            <View style={styles.social}>
                <TouchableOpacity>
                    <Text>
                    <SocialIcon
                      type='facebook'
                    />
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text>
                    <SocialIcon
                      type='google'
                    />
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text>
                    <SocialIcon
                      type='linkedin'
                    />
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  
}

const styles = StyleSheet.create({
  container:{
    width:'75%',
    margin:10
  },
  contLogo:{
    alignItems:'center',
    marginTop:20
  },
 logo:{
  width: 150,
 },
 subLogo:{
  color :'#fff',
  fontSize:20,
  fontWeight:'bold',
  paddingTop:10,
 },
  formInput:{
    borderRadius:5,
    padding:4,
    marginBottom:10
  },
  button:{
    marginTop:10,
    backgroundColor:'#335c67',
    height:50,
    alignItems:'center',
    justifyContent:'center',
    width:130,
    borderRadius:10,
  },
  buttonText:{
    color:'#fff',
    fontWeight:'bold'
  },
  buttonText2:{
    color:'#d65a06',
    fontWeight:'bold',
  },
  contButton:{
    display:'flex',
    alignItems:'center',
   
  },
  contText:{
    alignItems:'center',
    padding:10
  },
  contInput:{
    position:'relative',
    top: 30
  },
  social:{
    display:'flex',
    justifyContent:'space-around',
    flexDirection:'row',
    padding:7,
  }

});


export default SignUpScreen;

