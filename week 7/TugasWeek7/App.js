import React from 'react';
import {SafeAreaView,StyleSheet,ScrollView, View,Text,StatusBar,} from 'react-native';
import LoginScreen from './src/screens/LoginScreen'
import Homescreen from './src/screens/Homescreen'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Profile from './src/screens/Profile';
import HomeDetail from './src/screens/HomeDetail';

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <>
       <StatusBar barStyle="dark-content" />
      {/* <SafeAreaView style={styles.container}>
         <LoginScreen/>
         <Homescreen />
      </SafeAreaView>  */}
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="HomeDetail" component={HomeDetail} />
          <Tab.Screen name="Home" component={Homescreen} />
          <Tab.Screen name="Profile" component={Profile} />
          
        </Tab.Navigator>


      </NavigationContainer>

    </>
  );
};

const styles = StyleSheet.create({
  container :{
  //  backgroundColor:'#222',
  backgroundColor:'#ffff', 
  height:'100%',
   alignItems:'center'
  }
});

export default App;
