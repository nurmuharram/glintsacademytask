import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';

import List from './src/screen/index';

function App() {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <List />
      </SafeAreaView>
    </>
  );
}

export default App;
