// import axios from 'axios';

export function apiFetchListAnnouncement() {
    // return axios({
    //   method: 'GET',
    //   url: 'http:/localhost:3000/announcements',
    // });
  
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          data: [
            {
              id: 1,
              announcement: '[Binar] Semangat Mini Project',
              related_products: ['BINAR'],
            },
            {
              id: 2,
              announcement: '[Glints] Jangan lupa commit dan push setiap hari',
              related_products: ['GLINTS'],
            },
            {
              id: 3,
              announcement: '[Binar] Deadline Mini Project 20 November 2020',
              related_products: ['BINAR'],
            },
            {
              id: 4,
              announcement:
                '[Facil] Tugas week 1 - week 6 disubmit maksimal 15 November 2020 (hari Minggu)',
              related_products: ['FACIL'],
            },
          ],
        });
      }, 3000);
    });
  }
  