import React, {useState} from 'react';

import {TextInput, View, Image, Button, StyleSheet, Text} from 'react-native';

export default function List(props) {
  const [nama, setnama] = useState(null);
  const [produk, setProduk] = useState(null);
  const [desc, setDesc] = useState(null);
  const [message, setMessage] = useState(null);

  const checkList = () => {
    if (!nama) {
      setMessage('Nama tidak boleh kosong');
    } else if (!produk) {
      setMessage('Produk tidak boleh kosong');
    } else if (!desc) {
      setMessage('Desc tidak boleh kosong');
    } else {
      setMessage('Berhasil');
    }
  };

  return (
    <View>
      <Image source={require('../icon/1.png')} style={styles.image} />

      <View style={styles.container}>
        <Text>Survei Kepuasan Pelanggan</Text>

        <TextInput
          placeholder="Masukkan Nama Anda"
          onChangeText={(text) => setnama(text)}
          value={nama}
        />

        <TextInput
          placeholder="Produk yang digunakan "
          onChangeText={(text) => setProduk(text)}
          value={produk}
        />

        <TextInput
          placeholder="Tulis Pendapatmu Disini"
          onChangeText={(text) => setDesc(text)}
          multiline={true}
          numberOfLines={5}
        />

        <Text>{message}</Text>
        <Button
          title="Kirim"
          onPress={() => checkList()}
          style={styles.button}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 100,
    height: 100,
    marginLeft: 160,
    margin: 20,
  },

  container: {
    alignItems: 'center',
    borderRadius: 2,
  },

  button: {
    width: 200,
  },
});
