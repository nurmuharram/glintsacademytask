import React from 'react';
import axios from 'axios';
import './App.css';

export default class App extends React.Component{
constructor(props) {
  super(props);
  this.state = {
    listNews: [],
  };
}

componentDidMount(){
  console.info('halo');
  axios({
  method: 'GET',  
  url:
    'http://newsapi.org/v2/everything?domains=wsj.com&apiKey=2abe0480cc6445949f2473429bffbe8d',
})
  .then((res) => {
    console.info('articles', res.data.articles);
    this.setState({ listNews: res.data.articles});
  })
  .catch((err) => {

  });
}  
render() {
  return( 
      <div className="container">
      <h1>   <div id='Header' className="title">News For You</div>     </h1> 
          <div>
          {this.state.listNews.map((news, index) =>(
          <div className="item" key={index}> 
           <div className="item-image">
             <img alt={news.title} src={news.urlToImage} />
           </div> 
             
             <div className="item-content">
              <div>{news.title}</div>
              <div className="author-date">
              <div className="author">{news.author}</div>
              <div> {news.publishedAt}</div>
             </div> 
              <div className="description"> {news.description}</div>
            </div>
            </div>
       ))}
    </div>
    </div>
 
    );
  }
}