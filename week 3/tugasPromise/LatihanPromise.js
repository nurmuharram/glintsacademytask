const request = require('request');
const requestPromise = require('request-promise');

const options = {
    url: 'https://api.github.com/repos/request/request',
    headers: {
        'User-Agent' : 'request'
    }
};

requestPromise(options)
.then(response =>{
        const info = JSON.parse(response);
        console.log(info.name);
        console.log(info.stargazers_count + " Stars");
        console.log(info.forks_count + " Forks");
})
.catch( err => {
  console.log('Error')
})