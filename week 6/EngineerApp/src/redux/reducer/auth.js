const initialState ={
    username : null,
    isLoggedin : false,

};

const auth = (state = initialState , action) => {
    switch(action.type){
        case 'LOGIN' : {
            return{
                username : action.username,
                isLoggedin : true,
            };
        }
        default:
            return state;
    }
   
}

export default auth;
