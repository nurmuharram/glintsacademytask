import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import axios from 'axios';

export default class Jokes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listJokes: [],
      selectedJokes: {
        id: '',
        jokes: ['', ''],
      },
    };
  }


  async getListJokes(){
   //hasil axios simpan di res (async await)
    try{
      const res = await axios({
        method: 'GET',
        url: 'http://localhost:5000/jokes',
      });
      //set state listjokde dari data res
      this.setState({listJokes: res.data});
        this.timerJokes = setInterval(() => {
          const randomNumber = Math.floor(
            Math.random() * this.state.listJokes.length,
          );
          this.setState({selectedJokes: this.state.listJokes[randomNumber]});
        }, 5000);
    }catch(err) {
     //penanganan error dari axios
      console.error(err);
    }
  }
 

  componentDidMount() {
   this.getListJokes()
    // call axios
    // axios({
    //   method: 'GET',
    //   url: 'http://localhost:5000/jokes',
    // })
    //   .then((res) => {
    //     this.setState({listJokes: res.data});
    //     this.timerJokes = setInterval(() => {
    //       const randomNumber = Math.floor(
    //         Math.random() * this.state.listJokes.length,
    //       );
    //       this.setState({selectedJokes: this.state.listJokes[randomNumber]});
    //     }, 5000);
    //   })
    //   .catch((err) => {
    //     console.error(err);
    //   });
  }

  componentWillUnmount() {
    clearInterval(this.timerJokes);
  }

  render() {
    return (
      <View>
        <View style={styles.jokesHeader}>
          <Text>Jokes For You</Text>
          <Text>See All</Text>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.itemPertanyaan}>
            {this.state.selectedJokes.jokes[0]}
          </Text>
          <Text style={styles.itemJawaban}>
            {this.state.selectedJokes.jokes[1]}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  jokesHeader: {
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
    padding: 20,
    alignItems: 'stretch',
    height: 130,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 15,
  },
  itemPertanyaan: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontWeight: 'bold',
  },
  itemJawaban: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontStyle: 'italic',
  },
});
