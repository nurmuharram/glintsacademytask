import React from 'react';
import axios from 'axios';
import {
  StyleSheet,
  FlatList,
  Image,
  Linking,
  TouchableOpacity,
  View,
} from 'react-native';

export default class Promotion extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listPromotions: [],
    };
  }

  componentDidMount() {
    // call axios
    // Promotions
    axios({
      method: 'GET',
      url: 'http://localhost:5000/promotions',
    })
      .then((res) => {
        this.setState({
          listPromotions: res.data,
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  render() {
    return (
      <View>
        <FlatList
          pagingEnabled={true}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={this.state.listPromotions}
          renderItem={({item}) => (
            <TouchableOpacity
              key={item.index}
              style={styles.itemContainer}
              onPress={() => {
                Linking.openURL(item.url);
              }}>
              <Image
                source={{uri: item.preview_image}}
                style={styles.imageCarousel}
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item) => String(item.id)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
    width: 330,
    height: 170,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 15,
  },
  imageCarousel: {
    width: 330,
    height: 170,
    borderRadius: 15,
  },
});
