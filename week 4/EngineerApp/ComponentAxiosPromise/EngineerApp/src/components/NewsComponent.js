import React from 'react';
import {Text, View, StyleSheet,Image} from 'react-native';
import axios from 'axios';

export default class News extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
    };
  }

  componentDidMount() {
    axios
      .get(
        'http://newsapi.org/v2/everything?q=apple&from=2020-11-01&to=2020-11-01&sortBy=popularity&apiKey=2ba737ac7f9b4e5aa2df4eeeb7730895',
      )
      .then((res) => {
        this.setState({
          listNews: res.data.articles,
        })
      }).catch((err) => {
        console.error('error catch data, check connection');
      });
  }

  render() {
    return (
      <View key='index'>
        {this.state.listNews.map((news, index) => (
          <View style={styles.itemContainer} key={index}>
            <View style={styles.contImage}>
              <Image source={{uri:news.urlToImage}} style={styles.itemImage} />
            </View>
            <View style={styles.itemContent}>
              <Text style={styles.title}>{news.title}</Text>
              <Text style={styles.itemText}>
                {news.author} {news.publishedAt}
              </Text>
            </View>
          </View>
        ))}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    padding: 5,
    marginLeft:20,

  },
  itemImage: {
    width: 50,
    height: 50,
    borderRadius:50,
  },
  contImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemContent: {
    padding: 10,
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    color: '#222',
  },
  itemText: {
    color: '#222',
    alignSelf: 'stretch',
    textAlign: 'left',
  },
});
