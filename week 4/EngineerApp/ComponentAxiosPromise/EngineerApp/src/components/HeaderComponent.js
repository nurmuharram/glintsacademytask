import React from 'react';
import { StyleSheet, Text, View,Image, SafeAreaView } from 'react-native';


export default function Header() {
  return (
    <SafeAreaView>
      <View style={styles.container}>
          <Text style={styles.text1}>DAILYNFO</Text>
          <View style={styles.contImage}>
            <View >
              <Text style={styles.text2}>Hello,</Text>
              <Text style={styles.text2}>Nur Muharram</Text>
            </View>
            <View>
              <Image source={{uri: 'https://pbs.twimg.com/profile_images/1263098058956345344/S9WHIGkq_400x400.jpg'}}
                     style={{width: 85, height: 85}} />

            </View>
          </View>
    </View>   
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  text1: {
    fontWeight:'bold',
    fontSize:30,

  },
  text2: {
    fontWeight:'bold',
    marginRight:10,
    textAlign: 'right',
  },
  avatar:{
    width:40,
    height:40,
  },
  contImage:{
    display:'flex',
    flexDirection:'row'
  },
  container:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center',
    paddingTop:10,
    backgroundColor:'blue',
    paddingBottom:10,
  }
});
