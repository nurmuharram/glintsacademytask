import * as React from 'react';
import {
  Text,
  View,
  SafeAreaView, Image, StyleSheet, Dimensions , ImageBackground} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import axios from 'axios';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

export default class Slider extends React.Component {


    constructor(props){
        super(props);
        this.state = {
          activeIndex:0,
          listImage: []
      }
    }

    componentDidMount(){
      axios.get('http://newsapi.org/v2/everything?q=apple&from=2020-11-01&to=2020-11-01&sortBy=popularity&apiKey=2ba737ac7f9b4e5aa2df4eeeb7730895')
      .then((res) => {
        this.setState({
          listImage: res.data.articles,
        });
      })
      .catch((err) => {
        console.error('Error Check Your Connection')
      });
    }

    _renderItem({item,index}){
     return (
       <View style={{height:160,margin:11}}>
         <Image source = {{uri: item.urlToImage}} style={styles.carousel}/>
          <Text style={styles.title}>{item.title}</Text>
       </View>

     )
 }

 render() {
     return (
       <View>
             <Carousel
               layout={"default"}
               ref={ref => this.carousel = ref}
               data={this.state.listImage}
               sliderWidth={SLIDER_WIDTH}
               itemWidth={ITEM_WIDTH}
               renderItem={this._renderItem}
               onSnapToItem = { index => this.setState({activeIndex:index}) } />
       </View>
     );
 }
}

const styles = StyleSheet.create({
carousel : {
 width:'100%',
 height:'100%',
 position:'relative',
 resizeMode:'cover',
 borderRadius:15,

},
title :{
  fontWeight: 'bold',
  fontSize: 15,
  padding:10,
  position: 'absolute', 
  top:50,
  color:'white',
  backgroundColor: 'rgba(34,34,34,0.8)',
  
}
});
