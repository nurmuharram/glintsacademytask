import React from 'react';
import axios from 'axios';
import {View, Text, Image, StyleSheet} from 'react-native';

export default class Weather extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            listWeather: [],
            selectedWeather: {}
        }
    }

    componentDidMount(){
        axios({
            method: 'GET',
            url: 'http://localhost:5000/weather'
        })
        .then((res) => {
            console.info('data length ', res.data.length);
            this.setState({ listWeather: res.data, selectedWeather: res.data[0] }, () => {
                setInterval(() => {
                    if (this.state.selectedWeather !== {}){
                        const index = this.state.listWeather.findIndex(result => result.location === this.state.selectedWeather.location)
                        this.setState({ selectedWeather: this.state.listWeather[index === this.state.listWeather.length - 1 ? 0 : index + 1]})
                    }
                }, 5000)
            })
        })
        .catch((err) => {
            //error
            console.error('Error weather')
        })
    }

    render(){
        const {selectedWeather} = this.state
        return(
            <View style={styles.container}>
                <View>
                    <Text style={styles.location}>{selectedWeather.location}</Text>
                </View>
                <View>
                    <Text>{selectedWeather.short_note}</Text>
                </View>
                <View style={styles.miniContainer}>
                    <View style={styles.itemContainer}>
                        <Text style={styles.itemContent}>Suhu:  {selectedWeather.suhu}</Text>
                        <Text style={styles.itemContent}>Kecepatan Angin:   {selectedWeather.angin}</Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <Text style={styles.itemContent}>Kelembaban:    {selectedWeather.kelembaban}</Text>
                        <Text style={styles.itemContent}>Presipitasi:   {selectedWeather.presipitasi}</Text>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#d3d3d3',
        borderColor: 'skyblue',
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'column',
        padding: 20,
        margin: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.58,
        shadowRadius: 12.00,
        elevation: 24,
    },
    location:{
        color: 'navy',
        fontSize: 30,
        fontWeight: 'bold',
        // fontFamily: 'Poppins'
    },
    miniContainer:{
        display: 'flex',
        flexDirection: 'row'
    },
    itemContainer:{
        flex: 1,
        padding: 5
    },
    itemContent:{
        color: '#222',
        fontSize: 12
    }
})
