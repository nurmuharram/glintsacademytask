import React from 'react';
import { StyleSheet, Text, View,Image, SafeAreaView, ScrollView } from 'react-native';
import Slider from '../components/SliderComponent';
import Weather from '../components/WeatherComponent';
import Header from '../components/HeaderComponent';
import News from '../components/NewsComponent';

export default function DashboardScreen() {
  return (
    <>
    	<Header/>
	<View style={styles.container}>
        <Slider />
        <Weather/>
        <ScrollView>
        	<News/>
        </ScrollView>
    </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#dcc6e0',
    height:'100%'
  },
});
