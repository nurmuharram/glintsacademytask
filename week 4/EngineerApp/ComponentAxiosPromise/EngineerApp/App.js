import React from 'react';
import {SafeAreaView,StyleSheet,ScrollView,View,Text,StatusBar,} from 'react-native';

import {Header,LearnMoreLinks,Colors,DebugInstructions,ReloadInstructions,} from 'react-native/Libraries/NewAppScreen';
import DashboardScreen from './src/screens/DashboardScreen';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View>
        <DashboardScreen/>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
 
});

export default App;
